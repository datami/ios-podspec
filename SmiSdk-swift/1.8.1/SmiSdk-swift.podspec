
Pod::Spec.new do |s|


  s.name         = "SmiSdk-swift"
  s.version = "1.8.1"
  s.summary      = "Smi SDK for Sponsored Data on iOS Devices by Datami Mobile Solutions Pvt Ltd."

  s.description  = <<-DESC
                  Smi SDK for Sponsored Data on iOS Devices by Datami Mobile Solutions Pvt Ltd.
                   DESC

  s.homepage     = "https://bitbucket.org/datami/pod-smisdk-swift"

  s.license      = { :type => "EULA", :file => "LICENSE" }


  
  s.author             = { "Datami" => "info@datami.com" }

  s.platform     = :ios, "9.0"


  s.source       = { :git => "https://bitbucket.org/datami/pod-smisdk-swift", :tag => s.version.to_s}


  s.ios.deployment_target = '9.0'
  s.ios.vendored_frameworks = 'SmiSdkFramework.framework'



end

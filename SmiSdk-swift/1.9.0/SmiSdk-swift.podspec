
Pod::Spec.new do |s|


  s.name         = "SmiSdk-swift"
  s.version = "1.9.0"
  s.summary      = "Smi SDK for Sponsored Data on iOS Devices by Datami Mobile Solutions Pvt Ltd."

  s.description  = <<-DESC
                  Smi SDK for Sponsored Data on iOS Devices by Datami Mobile Solutions Pvt Ltd.
                   DESC

  s.homepage     = "http://www.datami.com"

  s.license      = { :type => "EULA", :file => "LICENSE" }


  
  s.author             = { "Datami" => "info@datami.com" }

  s.platform     = :ios, "9.0"


  s.source       = { :http => "http://s3.amazonaws.com/sdk-pods-ga-releases.cloudmi.datami.com/pods/pod-smisdk-swift-1.9.0.zip"}


  s.ios.deployment_target = '9.0'
  s.ios.vendored_frameworks = 'pod-smisdk-swift/SmiSdkFramework.framework'



end

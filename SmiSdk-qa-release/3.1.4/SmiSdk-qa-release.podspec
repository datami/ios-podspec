
Pod::Spec.new do |s|


  s.name         = "SmiSdk-qa-release"
  s.version = "3.1.4"
  s.summary      = "Smi SDK for Sponsored Data on iOS Devices by Datami Mobile Solutions Pvt Ltd."

  s.description  = <<-DESC
                  Smi SDK for Sponsored Data on iOS Devices by Datami Mobile Solutions Pvt Ltd.
                   DESC

  s.homepage     = "https://bitbucket.org/datami/pod-smisdk-qa-release"

  s.license      = { :type => "EULA", :file => "LICENSE" }


  
  s.author             = { "Datami" => "info@datami.com" }

  s.platform     = :ios, "9.0"


  s.source       = { :git => "https://bitbucket.org/datami/pod-smisdk-qa-release", :tag => s.version.to_s}


  s.source_files  = "include/*.h"
  s.public_header_files = "include/*.h"


  s.preserve_paths = "libsmisdk.a"
  s.ios.vendored_library = "libsmisdk.a"
  s.requires_arc = true


end

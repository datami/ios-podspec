
Pod::Spec.new do |s|
  s.name         = "Smisdk-swift-combined"
  s.version = "4.0.7"
  s.summary      = "Smi SDK for Sponsored Data on iOS Devices by Datami Mobile Solutions Pvt Ltd."
  s.description  = <<-DESC
                  Smi SDK for Sponsored Data on iOS Devices by Datami Mobile Solutions Pvt Ltd.
                   DESC
  s.homepage     = "http://www.datami.com"
  s.license      = { :type => "EULA"}
  s.author             = { "Datami" => "info@datami.com" }

  s.platform     = :ios, "9.0"
  s.source       = { :http => "https://s3.amazonaws.com/sdk-pods-ga-releases.cloudmi.datami.com/pods/Smisdk-swift-combined-4.0.7.zip"}
  s.vendored_frameworks = 'SmiSdkFramework.xcframework'
end


Pod::Spec.new do |s|


  s.name         = "SmiSdk-swift-iphones"
  s.version = "4.0.7"
  s.summary      = "Smi SDK for Sponsored Data on iOS Devices by Datami Mobile Solutions Pvt Ltd."

  s.description  = <<-DESC
                  Smi SDK for Sponsored Data on iOS Devices by Datami Mobile Solutions Pvt Ltd.
                   DESC

  s.homepage     = "http://www.datami.com"

  s.license      = { :type => "EULA"}


  
  s.author             = { "Datami" => "info@datami.com" }

  s.platform     = :ios, "9.0"


  s.source       = { :http => "https://s3.amazonaws.com/sdk-pods-ga-releases.cloudmi.datami.com/pods/pod-smisdk-swift-iphones-4.0.7.zip"}


  s.ios.deployment_target = '9.0'
  s.ios.vendored_frameworks = 'pod-smisdk-swift-iphones/SmiSdkFramework.framework'


  s.pod_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
  s.user_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }  



end

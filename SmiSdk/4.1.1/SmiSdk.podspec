
Pod::Spec.new do |s|


  s.name         = "SmiSdk"
  s.version = "4.1.1"
  s.summary      = "Smi SDK for Sponsored Data on iOS Devices by Datami Mobile Solutions Pvt Ltd."

  s.description  = <<-DESC
                  Smi SDK for Sponsored Data on iOS Devices by Datami Mobile Solutions Pvt Ltd.
                   DESC

  s.homepage     = "https://www.datami.com"

  s.license      = { :type => "EULA"}


  
  s.author       = { "Datami" => "info@datami.com" }

  s.platform     = :ios, "9.0"


  s.source       = { :http => 'https://s3.amazonaws.com/sdk-pods-ga-releases.cloudmi.datami.com/pods/pod-smisdk-objc-4.1.1.zip'}


  s.source_files  = "pod-smisdk-objc/include/*.h"
  s.public_header_files = "pod-smisdk-objc/include/*.h"


  s.preserve_paths = "pod-smisdk-objc/libsmisdk.a"
  s.ios.vendored_library = "pod-smisdk-objc/libsmisdk.a"
  s.requires_arc = true

  s.pod_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
  s.user_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }  


end
